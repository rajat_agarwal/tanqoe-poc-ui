import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { SoftwareEntity } from "app/modals/software.entity";
import { UserDetailsEntity } from "app/modals/UserDetails.Entity";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SoftwareDashboardService {
  private readonly covidUrl = `http://10.53.105.46:9091/api/v2/applications/1/detail`;
  private readonly zoomUrl = `http://10.53.105.46:9091/api/v2/zoom-users/1`;
  constructor(private http: HttpClient) {}

  getSoftwareById(): Observable<SoftwareEntity> {
    return this.http.get<SoftwareEntity>(this.covidUrl);
  }

  getUserDetails(): Observable<UserDetailsEntity> {
    return this.http.get<UserDetailsEntity>(this.zoomUrl);
  }
}
