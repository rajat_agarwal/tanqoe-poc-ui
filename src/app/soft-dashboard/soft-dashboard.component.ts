import { Component, OnInit } from "@angular/core";
import { DataEntity } from "app/modals/data.entity";
import { SoftwareEntity } from "app/modals/software.entity";
import { UserDetailsEntity } from "app/modals/UserDetails.Entity";
import { SoftwareDashboardService } from "app/services/software-dashboard.service";
import * as Chartist from "chartist";

@Component({
  selector: "soft-dashboard",
  templateUrl: "./soft-dashboard.component.html",
  styleUrls: ["./soft-dashboard.component.scss"],
})
export class SoftDashboardComponent implements OnInit {
  activeUser: number[];
  TotalUser: any;
  private costCur: number;
  private costYtd: number;
  private LatestData: any = new DataEntity();
  private zoom: any = new SoftwareEntity();
  userDetails: any = new UserDetailsEntity();
  // userDetails: UserDetailsEntity | undefined;

  constructor(private softwareService: SoftwareDashboardService) {}
  startAnimationForLineChart(chart) {
    let seq: any, delays: any, durations: any;
    seq = 0;
    delays = 80;
    durations = 500;

    chart.on("draw", function (data) {
      if (data.type === "line" || data.type === "area") {
        data.element.animate({
          d: {
            begin: 600,
            dur: 700,
            from: data.path
              .clone()
              .scale(1, 0)
              .translate(0, data.chartRect.height())
              .stringify(),
            to: data.path.clone().stringify(),
            easing: Chartist.Svg.Easing.easeOutQuint,
          },
        });
      } else if (data.type === "point") {
        seq++;
        data.element.animate({
          opacity: {
            begin: seq * delays,
            dur: durations,
            from: 0,
            to: 1,
            easing: "ease",
          },
        });
      }
    });

    seq = 0;
  }
  startAnimationForBarChart(chart) {
    let seq2: any, delays2: any, durations2: any;

    seq2 = 0;
    delays2 = 80;
    durations2 = 500;
    chart.on("draw", function (data) {
      if (data.type === "bar") {
        seq2++;
        data.element.animate({
          opacity: {
            begin: seq2 * delays2,
            dur: durations2,
            from: 0,
            to: 1,
            easing: "ease",
          },
        });
      }
    });

    seq2 = 0;
  }
  ngOnInit() {
    /* ----------====     (Completed Tasks) Application Usage Chart initialization    ====---------- */

    this.softwareService.getSoftwareById().subscribe((data: any) => {
      this.zoom = data;
      this.barChart();
      this.calculateData();

      console.log(this.zoom);
    });

    this.getZoomUser();
    // console.log(this.zoom);

    this.lineChart();
  }

  lineChart() {
    const dataCompletedTasksChart: any = {
      labels: [
        "Dec'21",
        "",
        "",
        "Mar'22",
        "",
        "",
        "Jun'22",
        "",
        "",
        "Sep'22",
        "",
        "",
      ],
      series: [
        [
          3600, 3800, 4200, 4300, 4400, 4800, 5300, 5400, 5500, 5800, 6000,
          6400,
        ],
      ],
    };

    const optionsCompletedTasksChart: any = {
      lineSmooth: Chartist.Interpolation.cardinal({
        tension: 0,
      }),
      low: 0,
      high: 8000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
      chartPadding: { top: 0, right: 0, bottom: 0, left: 10 },
    };

    var completedTasksChart = new Chartist.Line(
      "#completedTasksChart",
      dataCompletedTasksChart,
      optionsCompletedTasksChart
    );

    // start animation for the Completed Tasks Chart - Line Chart
    this.startAnimationForLineChart(completedTasksChart);
  }

  barChart() {
    const label: any = [];
    const series: any = [];

    this.zoom.activeUsers.forEach((element) => {
      label.push(element.label);
      series.push(element.count);
    });

    /* ----------======     (Emails Subscription) Active User Chart initialization    ======---------- */

    var datawebsiteViewsChart = {
      labels: label,
      // [
      //   "Dec'21",
      //   "",
      //   "",
      //   "Mar'22",
      //   "",
      //   "",
      //   "Jun'22",
      //   "",
      //   "",
      //   "Sep'22",
      //   "",
      //   "",
      // ],
      series: [
        series,
        [2000, 3000, 4000, 4000, 3000, 5200, 6800, 3400, 3000, 2000],
      ],

      // [
      //   [330, 320, 310, 310, 320, 298, 292, 316, 320, 330, 310, 320],
      //   [20, 30, 40, 40, 30, 52, 68, 34, 30, 20, 40, 30],
      // ],
    };
    var optionswebsiteViewsChart = {
      axisX: {
        showGrid: false,
      },
      low: 0,
      high: 30000,
      chartPadding: { top: 0, right: 5, bottom: 0, left: 10 },
    };
    var responsiveOptions: any[] = [
      [
        "screen and (max-width: 640px)",
        {
          seriesBarDistance: 5,
          axisX: {
            labelInterpolationFnc: function (value) {
              return value[0];
            },
          },
        },
      ],
    ];
    var websiteViewsChart = new Chartist.Bar(
      "#websiteViewsChart",
      datawebsiteViewsChart,
      optionswebsiteViewsChart,
      responsiveOptions
    );

    //start animation for the Emails Subscription Chart
    this.startAnimationForBarChart(websiteViewsChart);
  }

  calculateData() {
    this.LatestData = this.zoom.activeUsers[0];
    this.activeUser = this.LatestData.count;
    let a: any = this.activeUser;
    this.TotalUser = a + 30;
    console.log(this.activeUser);
  }

  getZoomUser() {
    this.softwareService.getUserDetails().subscribe((data) => {
      this.userDetails = data;
      console.log(data);
      console.log(this.userDetails);
    });
  }
}
