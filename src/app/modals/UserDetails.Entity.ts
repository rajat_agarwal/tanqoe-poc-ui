export class UserDetailsEntity {
  id: number;
  recordedTime: string;
  phone: string;
  firstName: string;
  lastName: string;
  email: string;
  lastLoggedInTime: string;
  status: string;
}
