import { DataEntity } from "./data.entity";

export class SoftwareEntity {
  id: number;
  appName: string;
  activeUsers: DataEntity[];
}
