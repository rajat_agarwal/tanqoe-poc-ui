import { Component, OnInit } from "@angular/core";
import { SoftwareDashboardService } from "app/services/software-dashboard.service";

@Component({
  selector: "app-table-list",
  templateUrl: "./table-list.component.html",
  styleUrls: ["./table-list.component.css"],
})
export class TableListComponent implements OnInit {
  totalUser: any;

  constructor(private softwareService: SoftwareDashboardService) {}

  ngOnInit() {
    this.softwareService.getSoftwareById().subscribe((data) => {
      this.totalUser = data.activeUsers[0].count + 30;
    });
  }
}
